FROM node:16-alpine AS build

WORKDIR /app/
ARG PAYPAL_CLIENT_ID
ENV REACT_APP_PAYPAL_CLIENT_ID $PAYPAL_CLIENT_ID
COPY public/ /app/public
COPY src/ /app/src
COPY package.json /app/

RUN npm install --force
# RUN npm run build

EXPOSE 3000 

CMD ["npm", "run", "start"]
