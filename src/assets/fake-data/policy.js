const policy = [
    {
        name: "Free Shipping",
        description: "Free ship minium orders > 239K",
        icon: "bx bx-shopping-bag"
    },
    {
        name: "Accept VISA, PAYPAL",
        description: "Purchased online by card",
        icon: "bx bx-credit-card"
    },
    {
        name: "VIP Customer",
        description: "Voucher for Vip",
        icon: "bx bx-diamond"
    },
    {
        name: "Support",
        description: "Refund store",
        icon: "bx bx-donate-heart"
    }
]

export default policy