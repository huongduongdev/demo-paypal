import React from 'react'
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'

import { useDispatch } from 'react-redux'

import { set } from '../redux/product-modal/productModalSlice'
import { addItem } from '../redux/shopping-cart/cartItemsSlide'
import Button from './Button'

import numberWithCommas from '../utils/numberWithCommas'
import { useHistory } from 'react-router-dom/cjs/react-router-dom'

const ProductCard = props => {
    const history = useHistory();
    const dispatch = useDispatch()

    const goToCart = () => {
        
            let newItem = {
                slug: props.slug,
               
                price: props.price,
                quantity: 1
            }
            if (dispatch(addItem(newItem))) {
                history.push('/cart')
            } else {
                alert('Fail')
            }
        
    }

    return (
        <div className="product-card">
            <Link to={`/catalog/${props.slug}`}>
                <div className="product-card__image">
                    <img src={props.img01} alt="" />
                    <img src={props.img02} alt="" />
                </div>
                <h3 className="product-card__name">{props.name}</h3>
                <div className="product-card__price">
                    {numberWithCommas(props.price)}
                    <span className="product-card__price__old">
                        <del>{numberWithCommas(399)}</del>
                    </span>
                </div>
            </Link>
            <div className="product-card__btn">
                <Button
                    size="sm"    
                    icon="bx bx-cart"
                    animate={true}
                    onClick={() => goToCart(props)}
                >
                    Add to cart
                </Button>
            </div>
        </div>
    )
}

ProductCard.propTypes = {
    img01: PropTypes.string.isRequired,
    img02: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    slug: PropTypes.string.isRequired,
}

export default ProductCard
