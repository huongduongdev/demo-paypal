import React from 'react'

import { Link } from 'react-router-dom'

import Grid from './Grid'

import logo from '../assets/images/Logo-2.png'

const footerAboutLinks = [
    {
        display: "About Us",
        path: "/about"
    },
    {
        display: "Contact",
        path: "/about"
    },
    {
        display: "Recuitment",
        path: "/about"
    },
    {
        display: "News",
        path: "/about"
    },
    {
        display: "Store address",
        path: "/about"
    }
]

const footerCustomerLinks = [
    {
        display: "Help & Contact Us",
        path: "/about"
    },
    {
        display: "Returns & Refunds",
        path: "/about"
    },
    {
        display: "Online Store",
        path: "/about"
    }
]
const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <Grid
                    col={4}
                    mdCol={2}
                    smCol={1}
                    gap={10}
                >
                    <div>
                        <div className="footer__title">
                            Supports
                        </div>
                        <div className="footer__content">
                            <p>
                                Contact Us: <strong>0123456789</strong>
                            </p>
                            <p>
                                Tracking : <strong>0123456789</strong>
                            </p>
                            <p>
                            Help & support: <strong>0123456789</strong>
                            </p>
                        </div>
                    </div>
                    <div>
                        <div className="footer__title">
                            About Store
                        </div>
                        <div className="footer__content">
                            {
                                footerAboutLinks.map((item, index) => (
                                    <p key={index}>
                                        <Link to={item.path}>
                                            {item.display}
                                        </Link>
                                    </p>
                                ))
                            }
                        </div>
                    </div>
                    <div>
                        <div className="footer__title">
                            Support Customer
                        </div>
                        <div className="footer__content">
                            {
                                footerCustomerLinks.map((item, index) => (
                                    <p key={index}>
                                        <Link to={item.path}>
                                            {item.display}
                                        </Link>
                                    </p>
                                ))
                            }
                        </div>
                    </div>
                    <div className="footer__about">
                        <p>
                            <Link to="/">
                                <img src={logo} className="footer__logo" alt="" />
                            </Link>
                        </p>
                        <p>
                        Curabitur placerat urna augue, id luctus sem imperdiet id. Nunc congue ac libero ut lacinia. In ultrices elementum ipsum, in tempus enim accumsan..
                        </p>
                    </div>
                </Grid>
            </div>
        </footer>
    )
}

export default Footer
