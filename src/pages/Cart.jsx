import React, { useEffect, useState } from 'react'

import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

import Helmet from '../components/Helmet'
import CartItem from '../components/CartItem'
import Button from '../components/Button'

import productData from '../assets/fake-data/products'
import numberWithCommas from '../utils/numberWithCommas'
import { PayPalButtons, usePayPalScriptReducer } from "@paypal/react-paypal-js";

const Cart = () => {
    const cartItems = useSelector((state) => state.cartItems.value)

    const [cartProducts, setCartProducts] = useState(productData.getCartItemsInfo(cartItems))

    const [totalProducts, setTotalProducts] = useState(0)

    const [totalPrice, setTotalPrice] = useState(0)
    
    useEffect(() => {
        setCartProducts(productData.getCartItemsInfo(cartItems))
        setTotalPrice(cartItems.reduce((total, item) => total + (Number(item.quantity) * Number(item.price)), 0))
        setTotalProducts(cartItems.reduce((total, item) => total + Number(item.quantity), 0))
    }, [cartItems])



      const [{ isPending }] = usePayPalScriptReducer();
      const paypalbuttonTransactionProps = {
        style: { layout: "vertical" },
        createOrder(data, actions) {
          
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  currency_code:'USD',
                  value: totalPrice
                }
              }
            ]
          });
        },
        onApprove(data, actions) {
          /**
           * data: {
           *   orderID: string;
           *   payerID: string;
           *   paymentID: string | null;
           *   billingToken: string | null;
           *   facilitatorAccesstoken: string;
           * }
           */
          return actions.order.capture({}).then((details) => {
            alert(
              "Transaction completed by" +
                (details?.payer.name.given_name ?? "No details")
            );
    
            alert("Data details: " + JSON.stringify(data, null, 2));
          });
        },
        
      };

    return (
        <Helmet title="Cart">
            <div className="cart">
                <div className="cart__info">
                    <div className="cart__info__txt">
                        <p>
                            You have <span style={{color:'#4267b2',fontWeight:'bold'}}>{totalProducts}</span> item in cart
                        </p>
                        <div className="cart__info__txt__price">
                            <span>Total:</span> <span>{numberWithCommas(Number(totalPrice))}</span>
                        </div>
                    </div>
                    <div className="cart__info__btn">
                    {isPending  ? (
                      <div>Loading...</div>
                    ) : (
                      <div className="w-full">
                      <PayPalButtons forceReRender={[totalPrice]} { ...paypalbuttonTransactionProps} />
                      </div>
                    )}
                      
                      
                        <Link to="/catalog">
                            <Button size="block">
                                Continue
                            </Button>
                        </Link>
                        
                    </div>
                </div>
                <div className="cart__list">
               
                  
                    {
                        cartProducts.map((item, index) => (
                            <CartItem item={item} key={index}/>
                        ))
                    }
                </div>
            </div>
        </Helmet>
    )
}

export default Cart
