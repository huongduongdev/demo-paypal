import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';

import { store } from './redux/store'
import { Provider } from 'react-redux'

import './assets/boxicons-2.0.7/css/boxicons.min.css'
import './sass/index.scss'

import Layout from './components/Layout'
import { PayPalScriptProvider,
  } from "@paypal/react-paypal-js";

ReactDOM.render(
  <React.StrictMode>
  <PayPalScriptProvider
  options={{ clientId: process.env.REACT_APP_PAYPAL_CLIENT_ID }}
  
 >
    <Provider store={store}>
      <Layout />
    </Provider>
   </PayPalScriptProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
